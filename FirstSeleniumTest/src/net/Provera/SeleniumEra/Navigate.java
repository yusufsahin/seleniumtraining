package net.Provera.SeleniumEra;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Navigate {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\work\\chromedriver.exe");
		WebDriver driver= new ChromeDriver();
		driver.get("https://www.google.com.tr");
		
		System.out.println(driver.getTitle());
		
		driver.findElement(By.name("btnI")).click();
		System.out.println(driver.getCurrentUrl());
		//element.sendKeys("hello world\n");
		//element.submit();
        Thread.sleep(4000);
		driver.navigate().back();
		System.out.println(driver.getCurrentUrl());
		//System.out.println(driver.getPageSource());
		Thread.sleep(4000);
		driver.navigate().forward();
		
		Thread.sleep(4000);
		driver.navigate().back();
		
		Thread.sleep(4000);
		driver.navigate().refresh();
		driver.close();
		driver.quit();
		
	}

}
