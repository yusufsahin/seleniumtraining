package net.provera.po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class POLogin {
	public POLogin(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	WebDriver driver;
	
	/*By username= By.id("Name");
	By password =By.id("Password");
	By login = By.xpath("/html/body/form/button");
	By allproducts = By.xpath("/html/body/div[1]/div[1]/h4");*/
	
	@FindBy(id="Name")
	WebElement UserName;
	
	@FindBy(id="Password")
	WebElement Password;
	
	@FindBy(xpath="/html/body/form/button")
	WebElement LogIn;

	@FindBy(xpath="/html/body/div[1]/div[1]/h4")
	WebElement AllProducts;
	
/*	public void setUserName(String username)
	{
		this.UserName.sendKeys(username);
	}
	*/
}
