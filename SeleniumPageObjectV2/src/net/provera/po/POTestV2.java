package net.provera.po;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class POTestV2 {

	@Test
	public void LoginTest() {
		System.setProperty("webdriver.chrome.driver", "C:\\work\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://sportsstore.provera.net/Account/Login?ReturnUrl=%2Fadmin%2Findex");
		POLogin poLogin= new POLogin(driver);
		
		poLogin.UserName.sendKeys("admin");
		//poLogin.setUserName("admin");
		poLogin.Password.sendKeys("Secret123$");
		poLogin.LogIn.submit();
		assertEquals("All Products", poLogin.AllProducts.getText());
	}

}
