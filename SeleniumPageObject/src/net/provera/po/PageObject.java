package net.provera.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageObject {
	
	public PageObject(WebDriver driver) {
		this.driver = driver;
	}
	
	WebDriver driver;
	
	By username= By.id("Name");
	By password =By.id("Password");
	By login = By.xpath("/html/body/form/button");
	By allproducts = By.xpath("/html/body/div[1]/div[1]/h4");
	
	public WebElement UserName()
	{
		return driver.findElement(username);
	}
	
	public WebElement Password()
	{
		return driver.findElement(password);
	}
	
	public WebElement LogIn()
	{
		return driver.findElement(login);
	}
	
	public WebElement AllProducts()
	{
		return driver.findElement(allproducts);
	}
	
}
