package net.provera.po;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class POTest {

	
	@Test
	public void Login() {
		System.setProperty("webdriver.chrome.driver", "C:\\work\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://sportsstore.provera.net/Account/Login?ReturnUrl=%2Fadmin%2Findex");
		PageObject po= new PageObject(driver);
		
		po.UserName().sendKeys("admin");
		po.Password().sendKeys("Secret123$");
		po.LogIn().submit();
		assertEquals("All Products", po.AllProducts().getText());
}
	

}
