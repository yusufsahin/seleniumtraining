package seleniumgluecode;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class test {
	
	WebDriver driver;
	@Given("^Open the Chrome and launch the application$")				
    public void open_the_Chrome_and_launch_the_application() throws Throwable							
    {		
        System.out.println("This Step open the Chrome and launch the application.");	
        System.setProperty("webdriver.chrome.driver", "C:\\work\\chromedriver.exe");
		
        driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 driver.get("https://www.facebook.com/login.php");
    }		

    @When("^Enter the Username and Password$")					
    public void enter_the_Username_and_Password() throws Throwable 							
    {		
       System.out.println("This step enter the Username and Password on the login page.");
      
   	driver.findElement(By.id("email")).sendKeys("aaaaa");
   	driver.findElement(By.id("pass")).sendKeys("bbbb");
   	driver.findElement(By.id("loginbutton")).click();
   	//WebDriverWait wait = new WebDriverWait(driver, 20);
    }		

    @Then("^Admin Page Appears")					
    public void Reset_the_credential() throws Throwable 							
    {    		
        System.out.println("Admin Page Appears");					
    }	
	
}
