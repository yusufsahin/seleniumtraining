package net.provera.SampleJUnit;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTests {

	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\work\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
    
	
	@Test
	public void LoginSuceess() {
		driver.get("https://www.facebook.com/login.php");
    	driver.findElement(By.id("email")).sendKeys("aaaaa");
    	driver.findElement(By.id("pass")).sendKeys("bbbb");
    	driver.findElement(By.id("loginbutton")).click();
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"error_box\"]/div[1]")));
    	
    	//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"globalContainer\"]/div[3]/div/div/div")));
    	assertTrue(true);
   	}
	
	@Test
	public void LoginInvalidPassword() {
		driver.get("https://www.facebook.com/login.php");
    	driver.findElement(By.id("email")).sendKeys("aaaaa");
    	driver.findElement(By.id("pass")).sendKeys("bbbb");
    	driver.findElement(By.id("loginbutton")).click();
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"error_box\"]/div[1]")));
    	
    	//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"globalContainer\"]/div[3]/div/div/div")));
    	assertTrue(true);
   	}

	@After
	public void tearDown() throws Exception {
		this.driver.quit();
	}

}
